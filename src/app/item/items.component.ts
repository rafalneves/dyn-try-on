import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { File } from '@nativescript/core/file-system';
import { isAndroid, Page } from '@nativescript/core/ui/page';
import { AR, ARFaceTrackingActions, ARTrackingFaceEventData } from 'nativescript-ar/ar-common';

import { HTTPService, MODELS_LOCATION } from './http.service';

//BROD36Black-Purple.scn
//BROD33Black-Pink-compressed.dae
//BROD33Black-Pink.dae
//BROD33Black-Pink.glb

const name = isAndroid ? 'BROD33Black-Pink.glb' : 'BROD33Black-Pink.dae';

@Component({
  selector: 'ns-items',
  moduleId: module.id,
  templateUrl: './items.component.html',
})

export class ItemsComponent implements OnInit {
  private faceTrackingActions: ARFaceTrackingActions;
  public Face: boolean = false;

  public error?: Error;

  @ViewChild('view', { static: false }) view: ElementRef<AR>;

  constructor(
    private page: Page,
    private http: HTTPService,
  ) {
    this.page.actionBarHidden = true;
  }

  ngOnInit(): void {}

  public fetchModelIfNeeded() {
    console.log('fetching...');
    this.http.fetchModel(name).then(() => {
      console.log('fetching... fetched');

      this.previewGlasses(name);
    }).catch((error) => {
      console.log('fetching... error: ', error);
    });
  }
  
  private previewGlasses(name: string) {
    if (this.faceTrackingActions) {
      console.log(MODELS_LOCATION + name, File.exists(MODELS_LOCATION + name));

      this.faceTrackingActions.addModel({
        name: MODELS_LOCATION + name,
        position: {
          x: 0,
          y: 0.0,
          z: 0.0
        },
        scale: 1,
      })
        .then(model => console.log('model:', model))
        .catch((error) => console.log('error:', error)); 
    }
  }
  
  trackingFaceDetected(args: ARTrackingFaceEventData): void {
    if (args.faceTrackingActions) { 
      this.faceTrackingActions = args.faceTrackingActions;

      this.fetchModelIfNeeded();  
    };
  }
}
