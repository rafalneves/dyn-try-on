import { Injectable } from '@angular/core';
import { knownFolders } from '@nativescript/core/file-system';
import { getFile } from '@nativescript/core/http';

export const MODELS_LOCATION = knownFolders.currentApp().path + '/assets/';

@Injectable()
export class HTTPService {
  constructor() { }

  public fetchModel(name: string): Promise<void> {
    return new Promise((resolve, reject) => {
      getFile('https://share.mixingpixels.com/' + name, MODELS_LOCATION + name).then(() => resolve()).catch(reject);
    });
  }
}